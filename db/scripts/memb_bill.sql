-- active bill/invoice for member

create table fi_bill(
  bill_no varchar(10) primary key, -- base-36
  acc_no number(12) not null, -- references acc.acc_no
  memb_type number(12) not null, -- references org_memb_type.type_id
  memb_id number(16) not null, -- references org_memb.memb_id
  charge_amt decimal(13,2), -- charge for the membership
  fine_amt decimal(13,2), -- monthley amount
  etc_amt decimal(13,2), -- other amount for charge in this bill
  etc_type  varchar(4), -- type of other amount in above
  -- audit
  create_by varchar(50) not null, 
  create_dt timestamp not null,
  updt_by varchar(50),
  updt_dt timestamp  
);