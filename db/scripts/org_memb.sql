-- org member

create table org_memb(
  memb_no primary key,
  org_id ,
  memb_type , -- references org_memb_type
  memb_stat , -- status of membership references ref_memb_stat, ACTV, SUSP, REMV
  -- approval
  approve_by , -- who approved, references usr
  approve_dt , -- date of approved
  expiry_dt date, -- if applicable
  -- payment info
  -- account info
  acc_bal decimal(13,2), -- account balance
  -- audit
  create_by , -- who added as this member
  create_dt timestamp not null, -- when this member added
  updt_by
  updt_dt
);