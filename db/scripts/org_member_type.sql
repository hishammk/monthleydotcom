-- type of membership in the org

create table org_memb_type(
  type_id number(12) primary key,
  org_id number(10) not null, -- references org.org_id
  memb_code varchar(5) not null,
  -- charges for this member
  charge_currency_code varchar(3) default 'MYR', -- currency of the payment
  charge_amt double(7,2),
  charge_freq number(3), -- frequency of the charge in weeks, (every 4 weeks constitute a month)
  charge_start_mnth number(2) default 1, -- start of the charge, in month, default to Jan
  charge_start_day number(2) default 1, -- start of the charge in day, default to 1st
  -- audit
);