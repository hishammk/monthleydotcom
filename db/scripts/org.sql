-- org that does the monthly collection

create table org(
  org_id number(10) primary key,
  org_name varchar(50) not null,
  org_type_code varchar(5) not null, -- type of orgs, references ref_org_type.org_type_code
  org_type_tags varchar(100), -- tags for the org
  org_stat varchar(4) not null default 'ACTV' , -- status of this org
  logo_img varchar(128), -- path of the org logo
  -- main address
  org_ctry varchar(2) not null default 'MY', --country code, references ref_ctry.ctry_code
  -- admin
  admin_id varchar(50) not null, -- admin of this org,  also used to approve membership, references usr.usr_id
  admin2_id varchar(50) not null, -- 2ndary admin
  -- billing
  acc_no varchar(10), -- account no, references acc.acc_no
  -- audit
  create_by varchar(16) not null,
  create_dt timestamp not null,
  updt_by varchar(16),
  updt_dt timestamp
);