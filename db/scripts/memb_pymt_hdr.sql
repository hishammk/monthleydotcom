-- member payment

create table memb_pymt_hdr(
  pymt_hdr_id number() primary key,
  -- payment type information
  pymt_type , -- references ref_pymt_type.type_code (only allow 1 type of payment for online payment)
  tot_amt monetary, -- total amount
  --audit
  create_by varchar(50) not null, -- who make the payment
  create_dt timestamp not null, -- date of the payment
  updt_by varchar(50),
  updt_dt timestamp  
);