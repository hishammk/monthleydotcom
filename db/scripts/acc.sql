-- org account

create table acc(
  acc_no varchar(10) primary key,
  acc_stat varchar(4) not null, -- account status, references ref_acc_stat.stat_code
  -- audit
  create_by varchar(50) not null, 
  create_dt timestamp not null,
  updt_by varchar(50),
  updt_dt timestamp
);