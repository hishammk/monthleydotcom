-- user application for membership, should support OAuth

create table usr_apply(
  usr_id varchar(50) primary key, -- normally an email
  usr_name varchar(100) not null,
  app_dt timestamp not null, -- date of application
  verify_code varchar(32) not null, -- GUID
  app_stat varchar(4) not null default 'UNVE', -- status of the request
  usr_stat_dt timestamp not null, -- last update date of the status
  -- address
  -- audit
);