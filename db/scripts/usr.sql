-- user for the app

create table usr(
  usr_id varchar(50) primary key, -- normally an email
  usr_name varchar(100) not null,
  alt_email varchar(50), --alternative email
  usr_stat varchar(4) not null default 'ACTV', -- ACTV, SUSP(ended), REMO(ved) (unverified user not stored here)
  usr_remark varchar(500),
  usr_stat_dt timestamp not null, -- last update date of the status
  -- address
  -- audit
);