-- account status reference table

create table ref_acc_stat(
  stat_code varchar(4) primary key,
  stat_name varchar(10) not null,
  descr varchar(500)
  -- audit
);